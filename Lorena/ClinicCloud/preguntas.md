# Preguntas

## WhatsApp
- Quisiera saber si la clinica LH usa estos detalles por WhatsApp:
    - ![ClinicCloud WhatsApp](images/LH_CC_WHATSAPP.jpg "ClinicCloud WhatsApp")
    - Por ejemplo, cuando se manda un WhatsApp al cliente, ¿se puede ver el "BUENOS DIAS" y todo?
    - ¿O salen los mensajes desde un móvil privado?
        - Quizá desde +34 611 43 59 50 ?
- Las instrucciones de ClinicCloud están [aquí](https://clinic-cloud.com/faq-soporte/recodatorio-de-citas-via-whatsapp/).
## SMS
- Os detalles parecen así:
    - ![ClinicCloud SMS](images/LL_CC_SMS.png "ClinicCloud SMS")
- Cuando se manda un SMS al cliente, ¿se puede ver el "CLÍNICA TGC" y todo?
- Tu ClinicCloud tiene una configuración de SMS que parece vacío:
    - ![ClinicCloud Citas SMS](images/LL_CC_CITAS_SMS.png "ClinicCloud Citas SMS")
- ¿Pagas a ClinicCloud para mensajes de SMS?
## GMail
- ¿Sabes cuando han salido estos mensajes?
    - ![ClinicCloudEmail](images/LL_CC_EMAIL.png "ClinicCloud Email")
- Tu ClinicCloud tiene una configuración de Citas por Email que parece vacío:
    - ![ClinicCloudCitasEmail](images/LL_CC_CITAS_EMAIL.png "ClinicCloud Citas Email")
- Y una configuración de Email que también parece vacío:
    - ![ClinicCloudConfigEmail](images/LL_CC_CONFIG_EMAIL.png "ClinicCloud Config Email")
- ¿Mandaís correos de GMail automaticamente por ClinicCloud?
- ¿Son los correos desde la cuenta "noema1989@gmail.com"?
- ¿Hablan los correos de "KAMBALAYA" ó no?
## Google Calendar
- ¿Hay vinculación o no?
- Al momento no veo ningún configuration de Google Calendar en Clinic Cloud, pero tal vez les puedo llamar.